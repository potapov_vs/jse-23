package com.nlmk.potapov.jse23;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Description {

    String parameterName;
    String parameterTypeName;
    Boolean hasValue;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterTypeName() {
        return parameterTypeName;
    }

    public void setParameterTypeName(String parameterTypeName) {
        this.parameterTypeName = parameterTypeName;
    }

    public Boolean getHasValue() {
        return hasValue;
    }

    public void setHasValue(Boolean hasValue) {
        this.hasValue = hasValue;
    }

    public List<List<Description>> getObjectDescription(List<Object> objects) throws IllegalAccessException {
        final List<List<Description>> resultList = new ArrayList<>();

        Class listObjectsClass = objects.get(0).getClass();
        for (Object object : objects) {
            if (!Objects.equals(object.getClass(), listObjectsClass)) {
                throw new IllegalArgumentException();
            }
        }

        for (Object object : objects) {
            List<Description> objectDescriptionList = new ArrayList<>();
            Class clazz = object.getClass();
            for (Field field : clazz.getDeclaredFields()) {
                Description objectDescription = new Description();
                objectDescription.setParameterName(field.getName());
                objectDescription.setParameterTypeName(field.getType().getName());
                field.setAccessible(true);
                objectDescription.setHasValue(!Objects.equals(field.get(object), null));
                objectDescriptionList.add(objectDescription);
            }
            while (!Objects.equals(clazz.getSuperclass(), null)) {
                clazz = clazz.getSuperclass();
                for (Field field : clazz.getDeclaredFields()) {
                    Description objectDescription = new Description();
                    objectDescription.setParameterName(field.getName());
                    objectDescription.setParameterTypeName(field.getType().getName());
                    field.setAccessible(true);
                    objectDescription.setHasValue(!Objects.equals(field.get(object), null));
                    objectDescriptionList.add(objectDescription);
                }
            }
            resultList.add(objectDescriptionList);
        }
        return resultList;
    }

    @Override
    public String toString() {
        return "Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterTypeName='" + parameterTypeName + '\'' +
                ", hasValue=" + hasValue +
                "}\n";
    }
}
