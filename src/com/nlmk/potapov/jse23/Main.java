package com.nlmk.potapov.jse23;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        Person person1 = new Person();
        Person person2 = new Person();

        person1.setFirstName("Иван");
        person1.setLastName("Смирнов");
        person1.setBirthDate(LocalDate.parse("1992-06-14"));

        person2.setFirstName("Игорь");
        person2.setBirthDate(LocalDate.parse("1991-04-09"));
        person2.setEmail("email2@пmail.com");

        Description description = new Description();

        List<List<Description>> result = description.getObjectDescription(Arrays.asList(person1, person2));
        System.out.println(result.toString());
    }

}
